/*Дано масив з числами. Створіти з нього новий масив, де залишаться тільки позитивні числа.
Створіть для цього допоміжну функцію isPositive (), яка аргументом буде приймати число і повертати true,
якщо число позитивне, і false - якщо негативне.
*/

var numbers = [3, -1, 4, -2, 2, 5];

function filterPositive(array) {
    var result = [];
    function isPositive(number) {
        return (number > 0);
    }
    array.forEach( function(element){
        if(isPositive(element)) {
            result.push(element);
        }
    });
    return result;
}

filterPositive(numbers);

/*Нехай у нас є тролейбусні квитки. Номер кожного квитка складається з 6 цифр.
Щасливий квиток - це квиток, сума перших трьох цифр якого дорівнює сумі других трьох цифр.
Написати функцію isHappy, яка перевіряє, щасливий квиток чи ні. Якщо це так - нехай функція повертає true, а якщо ні - false.
 */

var ticket = "777777";

function isHappy(string) {
    var TicketNumbers = parseInt( string.split("") );
    var leftSum = 0, rightSum = 0;
    for (var i = 0 ; i < TicketNumbers.length; i++) {
        if (i < 3) {
            leftSum += TicketNumbers[i];
        }
        rightSum += TicketNumbers[i];
    }
    return (leftSum == rightSum);
}

isHappy(ticket);

/*Потрібно реалізувати функцію strReplace(), яка буде здійснювати пошук та заміну.
Першим аргументом функція повинна приймати масив 'що міняємо', а другим параметром масив 'на що міняємо'.
Третім аргументом рядок, в якому міняємо. Функція повинна шукати в рядку елементи першого масиву і міняти їх
на відповідні елементи другого масиву.
*/

function strReplace(oldElements, newElements, string) {
    var array = string.split("");
    var result;
    oldElements.forEach(function(old, replaceIndex){
        array.forEach(function(char, charIndex){
            if(char == old) {
                array[charIndex] = newElements[replaceIndex];
            }
        });
    });
    return result = array.join("");
}

var test = "abd12asd2211";
var toReplace = ["1", "2"];
var newReplace = ["x", "y"];

strReplace(toReplace,newReplace, test);

/*Реалізувати функцію arrayFill(), яка буде заповнювати масив заданими значеннями.
Першим аргументом функція приймає значення, яким заповнювати масив, а другим - скільки елементів має бути в масиві.
Приклад: arrayFill ('x', 5) поверне масив ['x', 'x', 'x', 'x', 'x']. */

function arrayFill(char, arrayLength) {
    var result = [];
    for(var i = 0; i < arrayLength; i++){
        result.push(char);
    }
    return result;
}

arrayFill ('x', 5);

/*Використати функцію sort() для того, щоб пересортувати елементи масиву в випадковому порядку.*/

var randomMe = [1, 2, 3, 4 , 5];

function randomize(){
        return Math.floor(Math.random() + 0.5);
}
randomMe.sort(randomize);

/*Дано масив рядктів arr. Написати функцію unique (arr), яка повертає масив, що містить тільки унікальні елементи arr.*/

var test = ["aaa", "aa", "aa", "bb", "bb"];

function unique(array) {
    result = [];
    var notUnique;
    array.forEach(function(initValue){
        isUnique = true;
        result.forEach(function(uniqueValue){
            if (initValue == uniqueValue){
                isUnique = false;
            }
        });
        if (isUnique) {
            result.push(initValue);
        }
    });
    return result;
}

unique(test);