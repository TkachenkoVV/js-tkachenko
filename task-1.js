/*Задача 1:
Написати функцію buildTriangle(symbol, base), яка будує рівнобедрений 
трикутник із вказаних символів із заданою шириною основи. */
function buildTriangle(symbol, base) {
    var line = "";
    var emptycells= ~~(base/2); //ділимо націло на 2
    var rows = base/2;
    for (var i = 0; i <= rows; i++, emptycells--) { //цикл перебору стрічок
        for (var k = 0; k < emptycells; k++){ //пробіли в стрічці
            line += " ";
        }
        for (var j = 0; j < base - emptycells * 2; j++){ //символи в стрічці
            line += symbol;
        }
        console.log(line);
        line = "";
    }
}

buildTriangle(1, 20);

/*Задача 2: 
 Максимум. Напиcати функцію max, які беруть два аргументи, і повертає максимальний із них.*/
function max (arg1, arg2){
    return (arg2 > arg1) ? arg2 : arg1;
}

max (1, 3);

/*Задача 3:
Змінна місяць містить число з інтервалу від 1 до 12.
Визначити в яку пору року потрапить цей місяць (зима, літо, весна, осінь).*/

function getSeason (monthNumber) {
    var season = parseInt((monthNumber)/3);

    var SPRING = 1;
    var SUMMER = 2;
    var AUTUMN = 3;
    var WINTER = 4;

    switch (season) {
        case SPRING: console.log("spring");
            break;
        case SUMMER: console.log("summer");
            break;
        case AUTUMN: console.log("autumn");
            break;
        case WINTER: 
        case 0: console.log("winter");
            break;
        default: console.log("wrong month number")
            break;    
    }
}

getSeason(1);
getSeason(3);
getSeason(6);
getSeason("sss");

/*Задача 4:
4. В переменной year хранится год. 
Определите, является ли он високосным (в таком году есть 29 февраля).
Год будет високосным в двух случаях: либо он делится на 4, но при этом не делится на 100, либо делится на 400.
Так, годы 1700, 1800 и 1900 не являются високосными, так как они делятся на 100 и не делятся на 400.
Годы 1600 и 2000 - високосные, так как они делятся на 400.
*/

function leapYear (year) {
    if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0){
         return true;
        }
    return false;
}

console.log(leapYear(1700));
console.log(leapYear(2000));
console.log(leapYear(2004));

/* Задача 5:
Дана строка с цифрами, например, 'abcde'.
Проверьте, что первым символом этой строки является буква 'a'.
Если это так - выведите 'да', в противном случае выведите 'нет'.
*/

function getStringFirstSymbol (string, smbl){
    var str = string;
    var symbol = smbl || "a";
    if (str.charAt(0) == symbol) { 
        console.log("Yes");
    }
    else {
        console.log("No");
    }
}

getStringFirstSymbol("abcde", "a");
getStringFirstSymbol("abcde");

/* Задача 6:
    Дана строка, например, 'abcdef'. Переверните эту строку (сделайте из нее 'fedcba')
 */

function strReverse (string) {
    var initStr = string;
    var result = "";
    for (var i = initStr.length-1; i >= 0; i--){
        result += initStr[i];
    }
    return result;
}

strReverse("abcde");