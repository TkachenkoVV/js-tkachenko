/* Потрібно вивести день тиждня на українській або англійській мові. 
День задається порядковим номером в тиждні (від 1 до 7).
Має бути можливіть вказувати мову - параметр необовязковий, дефолтна мова - українська.*/

var week = {
	ua: ["понеділок", "вівторок", "середа", "четвер", "п'ятниця", "субота", "неділя"],
	eng: ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"],
	day: null,
	language: null,

	setLang: function () {
		this.language = prompt("Pls enter language (not required)");
		return this;
	},

	setDayNumber: function () {
		this.day = parseInt( prompt("Pls enter day of the week (required)") );
		return this;
	},
	
	getDay: function () {
		var res = 'null';
		this.lang = this.lang || "ua";
		if ( !(this.lang == "ua" || this.lang == "eng") ) {
			res = "Wrong language.";	
		}
		else {
			res = week[this.lang][this.day-1];
		}
		return res;
	}
};

week.setDayNumber().setLang().getDay();

/*Написати функцію створення генератора sequence (start, step). Вона при виклику повертає іншу функцію-генератор, 
яка при кожному виклику дає число на 1 більше, і так до нескінченності. Початкове число, з якого починати відлік,
і крок, задається при створенні генератора. Крок можна не вказувати, тоді він буде дорівнює одиниці.
Початкове значення за замовчуванням дорівнює 0. Генераторів можна створити скільки завгодно.*/

function sequence (start, step) {
	var generator = {
		strt: start || 0,
		stp: step || 1
	};
	generator.strt = start || 0;
	generator.stp = step || 1;
	return function (){
		return generator.strt += generator.stp;
	}
}; 

var generator = sequence(1,1);
generator();

/*Написати функцію pluck(), яка бере масив об'єктів і повертає масив значень певного поля:
	var characters = [
  	   { 'name': 'barney', 'age': 36 },
	   { 'name': 'fred', 'age': 40 }
	];
*/

var characters = [
  	   { 'name': 'barney', 'age': 36 },
	   { 'name': 'fred', 'age': 40 }
	];

function pluck (array, property){
	var buffer;
	var result = [];
	array.forEach(function(element) {
		result.push(element[property]);
	});
	return result;	
}

console.log(pluck(characters, "name"))

