/*Є два списки, які мають по 5 елементів. Потрібно реалізувати функцію по заміні 
елементів в списках, а саме перший елемент списку №1 вставити замість останнього 
елементу списку №2, відповідно останній елемент списку №2 має стати першим елементом 
списку№1. */

function swapListElements(){
    var ulElements = document.getElementsByTagName("ul");
    var buffer = ulElements[1].replaceChild(ulElements[0].firstElementChild, ulElements[1].lastElementChild);
    ulElements[0].insertBefore(buffer, ulElements[0].firstElementChild);
}

/*Потрібно реалізувати метод, який буде знаходити на сторінці всі посилання та копіювати їх в блок 
"сloned-anchor-list". Також при цьому змінювати тайтл сторінки на "Парсинг посилань сторінки". */

function cloneAnchors() {
    var anchorsList = document.getElementsByTagName("a");
    var anchorBuffer;
    var listLength = anchorsList.length;
    document.title = "Парсинг посилань сторінки";
    var anchorsBlock = document.createElement("div");
    anchorsBlock.className = "сloned-anchor-list";
    anchorsBlock.appendChild(document.createTextNode("Клоновані посилання"));
    anchorsBlock.appendChild(document.createElement("br"));
    document.body.appendChild(anchorsBlock);
    for (var i = 0; i < listLength; i++ ) {
        anchorBuffer = anchorsList[i].cloneNode(true);
        anchorsBlock.appendChild(anchorBuffer);  
        anchorsBlock.appendChild(document.createTextNode(" "))
    }
}

/*Потрібно написати функцію deleteTagsByClass(className), яка приймає назву класу, 
як аргумент та видаляє всі теги з документу які мають переданий тег. */

function deleteTagsByClass(className) {
    var deleteItems = document.getElementsByClassName(className);
    for (var i = deleteItems.length; i > 0; i --) {
        deleteItems[deleteItems.length-1].parentNode.removeChild(deleteItems[deleteItems.length-1]);
    }
}
    